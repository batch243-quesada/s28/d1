//// CRUD ////
// CRUD operations are the heart of any backen application
// mastering the CRUD operations is very essential for any developer
// This helps in building character and increasin exposure to logical statements that will help us manipulate
// A valuable developer and makes the work easier for us to deal with huge amounts of information

//// Inserting document (CREATE)
	// Insert one document
		// since MongoDB deals with objects as its structure for documents, we can easily create them by by providing objects into our methods
		// mongo shell also uses JS for its syntax which makes it convenient for us to understand its code
			// Syntax
			// db.collectionName.insertOne({object});
			// JavaScript:
			// object.object.method({object});

db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "1234321",
		email: "email@email.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})

// Insert Many
	// Syntax
	// db.collectionName.insertMany([{ObjectA}, {ObjectB}]);

db.users.insertMany([{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "122222",
		email: "shemail@email.com"
	},
	courses: ["Python", "React", "PHP"],
	department: "none"
}, {
	firstName: "neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "2222",
		email: "naemail@email.com"
	},
	courser: ["React", "Java", "HTML"],
	department: "none"
}])


// Finding Documents (READ)
// Find
// If multiple documents match the criteria for finding a document, only the first document that matches the search term will be returned
// this is based from the order that the documents are stored in a collection
// Syntax
// db.collectionName.find();
// db.collectionName.find({field: value});

// leaving the search criteria empty will retrieve all the documents
db.users.find();

db.users.find({firstName: "Stephen"});

// Pretty Method
// The pretty method allows us to be able to view the documents returned by our terminals in a better format
db.users.find({firstName: "Stephen"}).pretty();


// Finding documents with Multiple Parameters
// Syntax
// db.collectionName.find({fieldA: valueA, fieldB: valueB});
db.users.find({lastName: "Armstrong", age: 82}).pretty();


//// Updating Documents

// Updating a single document
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0, 
	contact: {
		phone: "00000000",
		email: "test@email.com"
	},
	course: [],
	department: "none"
})

// Just like the find method, updateOne method will only manipulate a single document. First document that matches the search criteria will be updated
// Syntax
// db.collectionName.updateOne({criteria}, {$set: {field:value}});
db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Bill"
		}
	}
);

db.users.updateOne(
	{firstName: "Bill"},
	{
		$set: {
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "1234",
				email: "bgemail@email.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);

// Updating Multiple Documents
// db.collectionName.updateMany({criteria}, {$set: {field : value}});
db.users.updateMany({department: "none"}, {
	$set: {
		department: "HR"
	}
});

//// Replacing Documents
// Replace One
// This can be used if replacing the whole document necessary
// Syntax
// db.collectionName.replaceOne({criteria}, {object});
db.users.replaceOne({firstName: "Bill"},{
		firstName: "Chris",
		lastName: "Mortel",
		age: 14,
		contact: {
			phone: "1222222",
			email: "cmemail@email.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations"
});

//// Deleting documents

db.users.insert({
	firstName: "test"
});

// Deleting One Document
// Syntax
// db.collectionName.deleteOne({criteria});
db.users.deleteOne({firstName: "test"});

// Deleting Many Documents
// Be careful when using the deleteMany method.
// If no search criteria is provided, it will delete all documents in the collection
// DO NO USE: db.collectionName.deleteMany();
// Syntax
// db.collectionName.deleteMany({criteria});
db.users.deleteMany({firstName: "Chris"});

//// Advanced Queries
// Query an embedded document
db.users.find({
	contact: {
		phone: "122222",
		email: "shemail@email.com"
	}
});

// query on nested field must be in JSON format
db.users.find({"contact.phone" : "122222"});

// querying an array with EXACT arrangement of elements
db.users.find({courses: ["Python", "React", "PHP"]});

// querying an array without regards to order and elements
db.users.find({courses:{$all: ["Python"]}});